package nl.utwente.di.temperatureCalculator;

public class Calculator {
    public double getTemperature(String temperature) {
        try {
            double temperatureInt = Integer.parseInt(temperature);
            return (((temperatureInt * 9) / 5) + 32);
        } catch (Exception e) {
            return 0;
        }
    }
}
